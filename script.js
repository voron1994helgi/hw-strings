// Task1
let str = 'шалаш';
let str2 = 'собака';
let str3 = 'Я може тоже хочу шоколад :(';

function isPalindrome(str) {
    let strLength = str.length;
    let strReverse = str.split('').reverse().join('');
    if (strReverse == str) {
        return true;
    } else {
        return false;
    }
}
console.log(isPalindrome(str));
console.log(isPalindrome(str2));

// Task2

function checkStringLength(str) {
    let strLength = str.length;
    if (strLength <= 20) {
        return true;
    } else {
        return false;
    }
}
console.log(checkStringLength(str));
console.log(checkStringLength(str2));
console.log(checkStringLength(str3));

// Task3

function checkAge(birthDate) {
    // Нам потрібна сьогоднішня дата :
    const today = new Date();

    // створюємо змінну , куди передамо значення дати народження юзера
    const userBirthDate = new Date(birthDate);

    // порахуємо мінус в мілісекундах
    const millisecondsDiff = today.getTime() - userBirthDate.getTime();

    // переведемо мілісекунди в роки
    const yearsOfUser = Math.floor(millisecondsDiff / (1000 * 60 * 60 * 24 * 365));

    // виводимо вік 
    return yearsOfUser;
}

// Приклад використання
const birthDate = prompt("Введіть дату народження (рік-місяць-день): ");
const age = checkAge(birthDate);
console.log(`Ваш повний вік: ${age} років`);
alert((`Ваш повний вік: ${age}.`));

// при перевірці цьої таски побачила , що достатньо ввести рік народження і воно прорахує скільки має виповнитися в цьому році.
// якщо ж я вводила дату повністю цього року , але майбутню ,то він рахував правильно .